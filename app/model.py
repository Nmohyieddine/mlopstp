from sklearn.datasets import load_iris, load_digits
from sklearn import svm
from sklearn.model_selection import train_test_split
import numpy as np

data = load_iris()
digits = load_digits()


class modelSVC:

    def __init__(self, gamma, c, test_size):
        self.clf = svm.SVC(gamma=gamma, C=c)
        self.gamma = gamma
        self.c = c
        self.test_size = test_size

    def train(self):
        X_train, X_test, y_train, y_test = self.crossvalidation()
        self.clf.fit(X_train, y_train)
        return self.clf

    def crossvalidation(self):
        return train_test_split(digits.data[:-1], digits.target[:-1], test_size=self.test_size,random_state=42)

    def predict(self,predictData):
        data = np.array(predictData.data)
        return {"prediction" : f"{self.clf.predict(data.reshape(1, -1))}"}

    def getmodel(self):
        return self.clf