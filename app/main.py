from fastapi import FastAPI
from model import modelSVC
from pydantic import BaseModel
from sklearn.metrics import accuracy_score

model: modelSVC
app = FastAPI()



class modelParam(BaseModel):
    gamma: float
    c: float
    test_size: float
class predictData(BaseModel):
    data: list

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.post("/train")
async def train(par: modelParam):
    global model
    model = modelSVC(par.gamma, par.c, par.test_size)
    model.train()
    print(model.clf)
    return {"model :" f"{model.clf}"}


@app.post("/predict")
async def predict(pre: predictData):
    global model
    return model.predict(pre)

@app.get("/accuracy")
async def predict():
    global model
    X_train, X_test, y_train, y_test = model.crossvalidation()
    y_pred = model.clf.predict(X_test)
    return {"accuracy:" f"{accuracy_score(y_test, y_pred)}"}
